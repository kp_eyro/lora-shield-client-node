# LoRa Shield Client Node
LoRa Shield Client Node using Arduino and Dragino LoRa shield RF95

To use RadioHead Library, first install RadioHead Packet Radio Library from this site: http://www.airspayce.com/mikem/arduino/RadioHead

Then install the library zip file in arduino in sketch->include library->add .zip library

References:
1. http://www.airspayce.com/mikem/arduino/RadioHead
2. http://wiki.dragino.com/index.php?title=Lora_Shield